import urllib.request
import argparse
import logging
import os

url = "https://selfadmin.911cr.net/10MB.zip"

file_name = '10MB.zip'
req = urllib.request.Request(url)
u = urllib.request.urlopen(req)
f = open(file_name, 'wb')
# meta = u.info()
print(u.getheader('Content-Length'))
file_size = int(u.getheader('Content-Length'))
remote_size = file_size
print("Downloading: %s Bytes: %s" % (file_name, file_size))

file_size_dl = 0
block_sz = 8192
while True:
    buffer = u.read(block_sz)
    if not buffer:
        break

    file_size_dl += len(buffer)
    f.write(buffer)
    status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
    status = status + chr(8)*(len(status)+1)
    print(status)

f.close()
local_size = os.stat(file_name).st_size
#local_reader = open(path, "rb")
print("Local: %s Bytes: %s" % (file_name, local_size))
print("Remote: %s Bytes: %s" % (file_name, remote_size))